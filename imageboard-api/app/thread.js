const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../fileDb');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', (req, res) => {
  const posts = db.getItems();
  return res.send(posts);
});

router.get('/:id', (req, res) => {
  const post = db.getItem(req.params.id);

  if(!post) {
    return  res.status(404).send({message: 'Not found'});
  }

  return res.send(post);
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if(!req.body.message) {
      return res.status(400).send({message: 'Message is required'});
    }

    const post = {
      author: req.body.author,
      message: req.body.message,
      image: req.body.image,
    };

    if(req.file) {
      post.image = req.file.filename;
    }

    await db.addItem(post);

    return res.send(post);
  } catch (e) {
    next(e);
  }
});

module.exports = router;