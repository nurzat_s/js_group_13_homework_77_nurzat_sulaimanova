const express = require('express');
const cors = require('cors');
const db = require('./fileDb');
const app = express();
const posts = require('./app/thread');

const port = 8000;


app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use(express.static('public'));
app.use('/thread', posts);

const run = async () => {
  await db.init();
}

app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});

run().catch(e => console.error(e));

