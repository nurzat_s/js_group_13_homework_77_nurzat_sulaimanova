export class Post {
  constructor(
    public id: string,
    public author: string,
    public message: string,
    public image: string
  ) {
  }
}

export interface PostData {
  [key: string]: any;
  author: string | null;
  message: string;
  image: File | null;
}
