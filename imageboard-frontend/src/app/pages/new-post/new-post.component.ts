import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PostsService } from '../../services/posts.service';
import { Post, PostData } from '../../models/post.model';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.sass']
})
export class NewPostComponent implements OnInit {
  @ViewChild('f') form! : NgForm;
  posts: Post[] = [];

  constructor(private postsService: PostsService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const postData: PostData = this.form.value;
    this.postsService.createPost(postData).subscribe(() => {
      this.postsService.getPosts().subscribe(posts => {
        this.posts = posts;
      })
    })


  }
}
