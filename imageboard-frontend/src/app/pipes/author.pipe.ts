import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../environments/environment';

@Pipe({
  name: 'author'
})
export class AuthorPipe implements PipeTransform {

  transform(value: string): string {
    if (value) {
      return environment.apiUrl + '/uploads' + value;
    } else {
      return 'Anonymous'
    }

  }
}
