import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post, PostData } from '../models/post.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class PostsService {

  constructor(private http: HttpClient) { }

  getPosts() {
    return this.http.get<Post[]>(environment.apiUrl + '/thread').pipe(
      map(response => {
        return response.map(postData => {
          return new Post(
            postData.id,
            postData.author,
            postData.message,
            postData.image
          )
        })
      }));
  }

  createPost(postData: PostData) {
    const formData = new FormData();

    Object.keys(postData).forEach(key => {
      if(postData[key] !== null) {
        formData.append(key, postData[key]);
      }
      console.log(formData)
    });

    return this.http.post(environment.apiUrl + '/thread', formData);
  }

}
